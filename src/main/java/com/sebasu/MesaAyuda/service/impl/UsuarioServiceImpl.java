package com.sebasu.MesaAyuda.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sebasu.MesaAyuda.dao.UsuarioDao;
import com.sebasu.MesaAyuda.entities.Perfil;
import com.sebasu.MesaAyuda.entities.Usuario;
import com.sebasu.MesaAyuda.entities.UsuarioPerfil;

@Service("userDetailsService")
public class UsuarioServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao.findByUsuario(username);

		if (usuario != null) {
			List<Perfil> authorities = new ArrayList<Perfil>();
			for (UsuarioPerfil usuarioPerfil : usuario.getPerfiles()) {
				authorities.add(usuarioPerfil.getPerfil());
			}
			UserDetails userDetails = new User(usuario.getUsuario(), usuario.getPassword(), usuario.isEnabled(),
					!usuario.isAccountNonExpired(), !usuario.isCredentialNonExpired(), !usuario.isAccountNonLocked(),
					authorities);
			return userDetails;
		} else {
			throw new UsernameNotFoundException("Usuario no encontrado.");
		}
	}

}

package com.sebasu.MesaAyuda.web.controller;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("api")
public class LogOutController {

	@Autowired
	JdbcTokenStore tokenStore;

	@GetMapping(value = "logout")
	public void logout(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Entre");
		new SecurityContextLogoutHandler().logout(request, null, null);
		try {
			response.sendRedirect(request.getHeader("referer"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "logout2")
	public @ResponseBody ResponseEntity<HttpStatus> logout2(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null) {
			try {
				String tokenValue = authHeader.replace("Bearer", "").trim();
				OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
				tokenStore.removeAccessToken(accessToken);
			} catch (Exception e) {
				return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
			}
		}

		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	public void deleteCookie(HttpServletRequest req, HttpServletResponse resp) {
		Cookie cookie = new Cookie("JSESSIONID", "");
		System.out.println(cookie.getName());
		cookie.setValue("");
		cookie.setPath("/");
		cookie.setMaxAge(0);
		resp.addCookie(cookie);

	}
}
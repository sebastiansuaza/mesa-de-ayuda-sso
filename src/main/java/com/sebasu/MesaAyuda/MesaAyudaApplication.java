package com.sebasu.MesaAyuda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@EnableAuthorizationServer
@EnableJpaAuditing
@EnableJpaRepositories
@EnableAutoConfiguration
public class MesaAyudaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MesaAyudaApplication.class, args);
	}

}

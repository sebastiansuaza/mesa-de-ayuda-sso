package com.sebasu.MesaAyuda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sebasu.MesaAyuda.entities.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long> {

	public Usuario findByUsuario(String usuario);

}

package com.sebasu.MesaAyuda.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "organizacion")
public class Organizacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "org_codigo")
	private Long codigo;

	@Column(name = "org_nombre", nullable = false)
	private String nombre;

	@Column(name = "org_color_hex", nullable = false)
	private String colorHex;

	public Organizacion() {

	}

	public Organizacion(Long codigo, String nombre, String colorHex) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.colorHex = colorHex;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColorHex() {
		return colorHex;
	}

	public void setColorHex(String colorHex) {
		this.colorHex = colorHex;
	}

	@Override
	public String toString() {
		return "Organizacion [codigo=" + codigo + ", nombre=" + nombre + ", colorHex=" + colorHex + "]";
	}

}

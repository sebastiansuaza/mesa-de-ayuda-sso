DROP TABLE IF EXISTS public.oauth_client_details;
CREATE TABLE public.oauth_client_details (
  	client_id character varying(255) NOT NULL,
  	resource_ids character varying(255),
  	client_secret character varying(255),
  	scope character varying(255),
  	authorized_grant_types character varying(255),
  	web_server_redirect_uri character varying(255),
  	authorities character varying(255),
  	access_token_validity integer,
  	refresh_token_validity integer,
  	additional_information character varying(4096),
  	autoapprove character varying(255),
  	CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id)
);

DROP TABLE IF EXISTS public.oauth_client_token;
CREATE TABLE public.oauth_client_token (
  	token_id character varying(255),
  	token bytea,
  	authentication_id character varying(255) NOT NULL,
  	user_name character varying(255),
  	client_id character varying(255),
  	CONSTRAINT oauth_client_token_pkey PRIMARY KEY (authentication_id)
);

DROP TABLE IF EXISTS public.oauth_access_token;
CREATE TABLE public.oauth_access_token (
  	token_id character varying(255),
  	token bytea,
  	authentication_id character varying(255) NOT NULL,
  	user_name character varying(255),
  	client_id character varying(255),
  	authentication bytea,
  	refresh_token character varying(255),
  	CONSTRAINT oauth_access_token_pkey PRIMARY KEY (authentication_id)
);

DROP TABLE IF EXISTS public.oauth_refresh_token;
CREATE TABLE public.oauth_refresh_token (
  	token_id character varying(255),
  	token bytea,
  	authentication bytea
);

DROP TABLE IF EXISTS public.oauth_code;
CREATE TABLE public.oauth_code (
  	code character varying(255),
  	authentication bytea
);

DROP TABLE IF EXISTS public.oauth_approvals;
CREATE TABLE public.oauth_approvals (
  	userid character varying(255),
  	clientid character varying(255),
  	scope character varying(255),
  	status character varying(10),
  	expiresat timestamp without time zone,
  	lastmodifiedat timestamp without time zone
);